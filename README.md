# API FARMACIAS DE TURNO  #

Permite listar las farmacias de turno por región y comuna. 

# Información Proyecto #

- **apiFarmacia** Version 1.0.0.
- **Java** Version 1.8.
- **Spring Boot** Version 2.2.3.
- **Gradle** 

## Estructura del Proyecto
```bash
    /src/main/java/apifarmacia
				/bean
				/controller
				/service
				/domain
```

# Instalación

1.- Descargar/Abrir IDE Eclipse

2.- Clonar Repositorio 
	
		git clone https://Latorre@bitbucket.org/Latorre/apifarmacia.git
		
3.- Importar repositorio desde eclipse con Glade

4.- Iniciar Glade (Levanta localhost - puerto: 9090 )


# Pruebas #

1.- Acceder por PostMan ( Documentación )

	https://documenter.getpostman.com/view/9168650/SWTA9HvX?version=latest

2.- Peticiones
	2.1.- Obtener comunas por región
	
			http://localhost:9090/comunas?region_id=7
			

	2.2.- Obtener Farmacias por región


			http://localhost:9090/farmacias?region_id=7
	





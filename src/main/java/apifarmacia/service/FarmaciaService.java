package apifarmacia.service;

import java.util.Arrays;

import org.apache.tomcat.util.json.JSONParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.deser.impl.ExternalTypeHandler.Builder;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import apifarmacia.bean.FarmaciaBean;
import domain.Farmacia;

/*
 * Servicio Farmacia 
 */
@Service
public class FarmaciaService {

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	/* 
	 * Obtener Farmacias por Region 
	 */
	public String obtenerFarmacias(Integer region_id) {
		
		//FarmaciaBean farmacia = new FarmaciaBean();
		RestTemplate res = new RestTemplate();
		String response = res.getForObject(
				"https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=" + region_id, String.class);

		return response;
	}
	
	/* 
	 * Obtener Comunas por Region 
	 */
	public String obtenerComunas(Integer id_region){
		RestTemplate res = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "multipart/form-data");

		MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
		bodyMap.add("reg_id", id_region.toString());

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(bodyMap, headers);

		String comunas = res.postForObject(
				"https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones",
				requestEntity, String.class);
		
		return comunas;
	}

	/* 
	 * Buscar Farmacias por region, comuna y local 
	 */
	public String buscarFarmacias(Integer region_id, Integer id_comuna, String nombreLocal) {
		String ListaFarmacias = obtenerFarmacias(region_id);
		//filtrar por id comuna
		
		//filtrar por nombre
		

		return ListaFarmacias;
	}


}